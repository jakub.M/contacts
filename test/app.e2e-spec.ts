import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication, ValidationPipe } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from './../src/app.module';
import { DatabaseService } from '@app/core/database/database.service';
import {
  IAddressDb,
  ICreateAddress,
} from '@app/core/database/collections/addresses/addresses-database.models';
import { faker } from '@faker-js/faker';
import { ContactPgRepository } from '@app/core/database/postgres/contacts/contacts-pg.repository';
import { ContactPg } from '@app/core/database/postgres/contacts/contacts-pg.entity';
import { AddressPgRepository } from '@app/core/database/postgres/addresses/addresses-pg.repository';

describe('App e2e', () => {
  jest.setTimeout(50000);
  let app: INestApplication;
  let databaseService: DatabaseService;
  const contactIds: string[] = [];

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    })
      .compile()
      .then();

    app = moduleFixture.createNestApplication();
    app.useGlobalPipes(
      new ValidationPipe({
        whitelist: true,
      }),
    );
    await app.init();

    databaseService = moduleFixture.get<DatabaseService>(DatabaseService);
  });

  it('should init app', () => {
    expect(app).toBeTruthy();
  });

  describe('ContactsController', () => {
    const badContact = {
      firstName: 'first',
      lastName: 'last',
      age: 55,
      email: 'bad.email',
      phoneNumber: '123123aaa',
    };

    const contact = {
      firstName: 'first',
      lastName: 'last',
      age: 55,
      email: 'bad.email@gmail.com',
      phoneNumber: '123123123',
    };

    it('/contacts (POST)', (done) => {
      request(app.getHttpServer())
        .post('/contacts/')
        .send(badContact)
        .expect(400, {
          statusCode: 400,
          message: [
            'email must be an email',
            'phoneNumber must match /\\d{9}/ regular expression',
          ],
          error: 'Bad Request',
        })
        .end(done);
    });

    it('/contacts (POST)', (done) => {
      request(app.getHttpServer())
        .post('/contacts/')
        .send(contact)
        .expect(201)
        .expect((response) => {
          const body = response.body;
          contactIds.push(body.id);

          expect(body).toEqual({
            ...contact,
            id: expect.any(String),
            addresses: [],
          });
        })
        .end(done);
    });

    it('/contacts/many (POST)', (done) => {
      const contacts = new Array(500).fill(contact);
      console.time('test');
      request(app.getHttpServer())
        .post('/contacts/many')
        .send({ contacts })
        .expect(201)
        .expect(() => {
          console.timeEnd('test');
        })
        .end(done);
    });

    it('/contacts/:id (GET)', async () => {
      const id = contactIds[0];
      await app.get(AddressPgRepository).save({
        city: faker.address.city(),
        address: faker.address.streetAddress(),
        postal_code: faker.address.zipCode(),
        contactId: id,
      });

      await request(app.getHttpServer())
        .get(`/contacts/${id}`)
        .expect(200)
        .expect((response) => {
          expect(response.body).toEqual({
            ...contact,
            id,
            addresses: [
              {
                address: expect.any(String),
                id: expect.any(String),
                city: expect.any(String),
                postalCode: expect.any(String),
              },
            ],
          });
        });
    });
  });

  describe('AddressesController', () => {
    const contactData = {
      firstName: 'first',
      lastName: 'last',
      age: 55,
      email: 'bad.email@gmail.com',
      phoneNumber: '123123123',
    };

    let contact: ContactPg;

    const getAddressDetails = (): ICreateAddress => ({
      city: faker.address.city(),
      address: faker.address.streetAddress(),
      postalCode: faker.address.zipCode(),
    });

    const ADDRESS_COUNT = 5;

    beforeAll(async () => {
      contact = await app.get(ContactPgRepository).createOne(contactData);
    });

    it('addresses/many/to/contact/:contactId (POST)', async () => {
      const addresses = new Array(ADDRESS_COUNT)
        .fill(null)
        .map(getAddressDetails);

      const { body } = await request(app.getHttpServer())
        .post(`/addresses/many/to/contact/${contact.id}`)
        .send(addresses)
        .expect(201);

      (body as IAddressDb[]).forEach((address) =>
        expect(address).toEqual({
          id: expect.any(String),
          city: expect.any(String),
          address: expect.any(String),
          postalCode: expect.any(String),
        }),
      );
      const createdAddressCount = await app
        .get(AddressPgRepository)
        .count({ where: { contactId: contact.id } });

      expect(createdAddressCount).toBe(ADDRESS_COUNT);
    });
  });

  afterAll((done) => {
    databaseService.contactsDeleteAll().subscribe(async () => {
      await app.close();
      done();
    });
  });
});
