import { AddressesDatabaseService } from '@app/core/database/collections/addresses/addresses-database.service';
import { AddressesPgService } from '@app/core/database/postgres/addresses/contacts-pg.service';
import { Module } from '@nestjs/common';
import { ContactsDatabaseService } from './collections/contacts/contacts-database.service';
import { DatabaseService } from './database.service';
import { ContactsPgService } from './postgres/contacts/contacts-pg.service';
import { PostgresModule } from './postgres/postgres.module';

@Module({
  imports: [PostgresModule],
  providers: [
    DatabaseService,
    { provide: ContactsDatabaseService, useExisting: ContactsPgService },
    { provide: AddressesDatabaseService, useExisting: AddressesPgService },
  ],
  exports: [DatabaseService],
})
export class DatabaseModule {}
