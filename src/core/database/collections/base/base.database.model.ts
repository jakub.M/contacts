export interface IBaseDbModel {
  id: string;
  createdDate: Date;
  updatedDate: Date;
}