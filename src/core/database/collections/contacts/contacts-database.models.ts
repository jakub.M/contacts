import { IAddressDb } from '@app/core/database/collections/addresses/addresses-database.models';

export interface IContactDb {
  id: string;
  createdDate: Date;
  updatedDate: Date;

  firstName: string;
  lastName: string;
  age: number;
  email: string;
  phoneNumber: string;

  addresses: IAddressDb[];
}

export interface ICreateContact {
  firstName: string;
  lastName: string;
  age: number;
  email: string;
  phoneNumber: string;
}
