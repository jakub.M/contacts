import {
  IAddressDb,
  ICreateAddress,
} from '@app/core/database/collections/addresses/addresses-database.models';
import { Injectable } from '@nestjs/common';
import { Observable } from 'rxjs';

@Injectable()
export abstract class AddressesDatabaseService {
  abstract getOne(contactId: string): Observable<IAddressDb>;

  abstract createOne(createDto: ICreateAddress): Observable<IAddressDb>;

  abstract createMany(
    createDtos: ICreateAddress[],
    contactId: string,
  ): Observable<IAddressDb[]>;

  abstract deleteAll(ids: string[]): Observable<unknown>;
}
