import { IBaseDbModel } from '@app/core/database/collections/base/base.database.model';

export interface IAddressDb extends ICreateAddress, IBaseDbModel {}

export interface ICreateAddress {
  city: string;
  address: string;
  postalCode: string;
}
