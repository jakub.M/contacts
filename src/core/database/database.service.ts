import {
  IAddressDb,
  ICreateAddress,
} from '@app/core/database/collections/addresses/addresses-database.models';
import { AddressesDatabaseService } from '@app/core/database/collections/addresses/addresses-database.service';
import { Injectable } from '@nestjs/common';
import { Observable } from 'rxjs';
import {
  IContactDb,
  ICreateContact,
} from './collections/contacts/contacts-database.models';
import { ContactsDatabaseService } from './collections/contacts/contacts-database.service';

@Injectable()
export class DatabaseService {
  constructor(
    private readonly contactsDatabaseService: ContactsDatabaseService,
    private readonly addressesDatabaseService: AddressesDatabaseService,
  ) {}

  contactsGetOne(contactId: string): Observable<IContactDb> {
    return this.contactsDatabaseService.getOne(contactId);
  }

  contactsCreateOne(createDto: ICreateContact): Observable<IContactDb> {
    return this.contactsDatabaseService.createOne(createDto);
  }

  contactsCreateMany(createDtos: ICreateContact[]): Observable<IContactDb[]> {
    return this.contactsDatabaseService.createMany(createDtos);
  }

  contactsDeleteAll(): Observable<unknown> {
    return this.contactsDatabaseService.deleteAll();
  }

  createManyAddresses(
    createDtos: ICreateAddress[],
    contactId: string,
  ): Observable<IAddressDb[]> {
    return this.addressesDatabaseService.createMany(createDtos, contactId);
  }
}
