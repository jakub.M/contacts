export interface IIdPg {
  id: string;
}
export interface IUpdatedCreatedPg extends IIdPg {
  created_date: Date;
  updated_date: Date;
}
