import { IUpdatedCreatedPg } from '@app/core/database/basees/base.model';
import IdPg from '@app/core/database/basees/id.baseEntity';
import { Column, Entity } from 'typeorm';

@Entity()
export class CreatedUpdatedPg extends IdPg implements IUpdatedCreatedPg {
  @Column({ type: 'timestamptz', default: () => 'CURRENT_TIMESTAMP' })
  created_date: Date;

  @Column({ type: 'timestamptz', default: () => 'CURRENT_TIMESTAMP' })
  updated_date: Date;
}
