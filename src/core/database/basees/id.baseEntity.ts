import { IIdPg } from '@app/core/database/basees/base.model';
import { BaseEntity, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export default class IdPg extends BaseEntity implements IIdPg {
  @PrimaryGeneratedColumn('uuid')
  id: string;
}
