import { CreatedUpdatedPg } from '@app/core/database/basees/createdUpdated.baseEntity';
import { IAddressPg } from '@app/core/database/postgres/addresses/addresses-pg.models';
import { ContactPg } from '@app/core/database/postgres/contacts/contacts-pg.entity';
import { Column, Entity, ManyToOne } from 'typeorm';

@Entity()
export class AddressPg extends CreatedUpdatedPg implements IAddressPg {
  @Column({ type: 'varchar', nullable: true })
  city: string;

  @Column({ type: 'varchar', nullable: true })
  address: string;

  @Column({ type: 'varchar', nullable: true })
  postal_code: string;

  @ManyToOne(() => ContactPg, { onDelete: 'CASCADE' })
  contact: ContactPg;

  @Column()
  contactId: string;
}
