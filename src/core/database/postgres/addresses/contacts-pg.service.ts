import {
  IAddressDb,
  ICreateAddress,
} from '@app/core/database/collections/addresses/addresses-database.models';
import { AddressesDatabaseService } from '@app/core/database/collections/addresses/addresses-database.service';
import { AddressPgDto } from '@app/core/database/postgres/addresses/addresses-pg.models';
import { AddressPgRepository } from '@app/core/database/postgres/addresses/addresses-pg.repository';
import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { from, map } from 'rxjs';
import { Observable } from 'rxjs';

@Injectable()
export class AddressesPgService implements AddressesDatabaseService {
  constructor(
    @InjectRepository(AddressPgRepository)
    private addressPgRepository: AddressPgRepository,
  ) {}

  getOne(contactId: string): Observable<IAddressDb> {
    return from(this.addressPgRepository.getOne(contactId)).pipe(
      map((contactPg) => {
        if (!contactPg)
          throw new NotFoundException(
            `Could not find contact by id ${contactId}`,
          );

        return new AddressPgDto(contactPg);
      }),
    );
  }

  createOne(createDto: ICreateAddress): Observable<IAddressDb> {
    return from(this.addressPgRepository.createOne(createDto)).pipe(
      map((contactPg) => new AddressPgDto(contactPg)),
    );
  }

  createMany(
    createDtos: ICreateAddress[],
    contactId: string,
  ): Observable<IAddressDb[]> {
    return from(
      this.addressPgRepository.createMany(createDtos, contactId),
    ).pipe(
      map((contactPg) =>
        contactPg.map((contactPg) => new AddressPgDto(contactPg)),
      ),
    );
  }

  deleteAll(ids: string[]): Observable<unknown> {
    return from(this.addressPgRepository.deleteAll(ids));
  }
}
