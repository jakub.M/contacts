import { ICreateAddress } from '@app/core/database/collections/addresses/addresses-database.models';
import { AddressPg } from '@app/core/database/postgres/addresses/addresses-pg.entity';
import { EntityRepository, Repository } from 'typeorm';

@EntityRepository(AddressPg)
export class AddressPgRepository extends Repository<AddressPg> {
  public async getOne(contactId: string): Promise<AddressPg | undefined> {
    return await this.findOne(contactId);
  }

  public async createMany(
    createAddresses: ICreateAddress[],
    contactId: string,
  ): Promise<AddressPg[]> {
    const response: AddressPg[] = [];

    for (const createAddress of createAddresses) {
      const address = new AddressPg();

      address.city = createAddress.city;
      address.address = createAddress.address;
      address.postal_code = createAddress.postalCode;
      address.contactId = contactId;

      const savedData = await address.save();
      response.push(savedData);
    }

    return response;
  }

  public async createOne(createContact: ICreateAddress): Promise<AddressPg> {
    const contact = new AddressPg();
    contact.city = createContact.city;
    return await contact.save();
  }

  public async deleteAll(ids: string[]): Promise<unknown> {
    return this.delete(ids);
  }
}
