import { IAddressDb } from '@app/core/database/collections/addresses/addresses-database.models';

export interface IAddressPg {
  id: string;
  created_date: Date;
  updated_date: Date;

  city: string;
  address: string;
  postal_code: string;
}

export class AddressPgDto implements IAddressDb {
  id: string;
  createdDate: Date;
  updatedDate: Date;

  city: string;
  address: string;
  postalCode: string;

  constructor(entity: IAddressPg) {
    const init: IAddressDb = {
      id: entity.id,
      createdDate: entity.created_date,
      updatedDate: entity.updated_date,

      city: entity.city,
      address: entity.address,
      postalCode: entity.postal_code,
    };

    Object.assign(this, init);
  }
}
