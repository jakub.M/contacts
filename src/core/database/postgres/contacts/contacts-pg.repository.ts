import { EntityRepository, Repository } from 'typeorm';
import { ICreateContact } from '../../collections/contacts/contacts-database.models';
import { ContactPg } from './contacts-pg.entity';

@EntityRepository(ContactPg)
export class ContactPgRepository extends Repository<ContactPg> {
  public async getOne(contactId: string): Promise<ContactPg | undefined> {
    return await this.findOne(contactId, { relations: ['addresses'] });
  }

  public async createOne(createContact: ICreateContact): Promise<ContactPg> {
    const contact = new ContactPg();
    const nowDate = new Date();

    contact.first_name = createContact.firstName;
    contact.last_name = createContact.lastName;
    contact.age = createContact.age;
    contact.email = createContact.email;
    contact.phoneNumber = createContact.phoneNumber;

    contact.created_date = nowDate;
    contact.updated_date = nowDate;

    contact.addresses = [];

    return await contact.save();
  }

  public async createMany(
    createContacts: ICreateContact[],
  ): Promise<ContactPg[]> {
    return this.save(
      createContacts.map((contact) => ({
        first_name: contact.firstName,
        last_name: contact.lastName,
        age: contact.age,
        email: contact.email,
        phoneNumber: contact.phoneNumber,
        addresses: [],
      })),
    );
  }

  public async deleteAll(ids: string[]): Promise<unknown> {
    return this.delete(ids);
  }
}
