import { CreatedUpdatedPg } from '@app/core/database/basees/createdUpdated.baseEntity';
import { AddressPg } from '@app/core/database/postgres/addresses/addresses-pg.entity';
import { Column, Entity, OneToMany } from 'typeorm';
import { IContactPg } from './contacts-pg.models';

@Entity()
export class ContactPg extends CreatedUpdatedPg implements IContactPg {
  @Column({ type: 'varchar' })
  first_name: string;

  @Column({ type: 'varchar' })
  last_name: string;

  @Column({ type: 'integer' })
  age: number;

  @Column({ type: 'varchar' })
  email: string;

  @Column({ type: 'varchar' })
  phoneNumber: string;

  @OneToMany(() => AddressPg, (address) => address.contact)
  addresses: AddressPg[];
}
