import {
  IContactDb,
  ICreateContact,
} from '@app/core/database/collections/contacts/contacts-database.models';
import { ResponseAddressDto } from '@app/modules/addresses/addAddress.dto';
import { Type } from 'class-transformer';
import {
  ArrayNotEmpty,
  IsEmail,
  IsInt,
  IsString,
  Matches,
  ValidateNested,
} from 'class-validator';

export class CreateContactDto implements ICreateContact {
  @IsString() firstName: string;
  @IsString() lastName: string;
  @IsInt() age: number;
  @IsEmail() email: string;
  @Matches(/\d{9}/) phoneNumber: string;
}

export class CreateManyContactsDto {
  @ArrayNotEmpty()
  @ValidateNested({ each: true })
  @Type(() => CreateContactDto)
  contacts: ICreateContact[];
}

export class ResponseContactDto {
  id: string;
  firstName: string;
  lastName: string;
  age: number;
  email: string;
  phoneNumber: string;
  addresses: ResponseAddressDto[];

  constructor(contact: IContactDb) {
    this.id = contact.id;
    this.firstName = contact.firstName;
    this.lastName = contact.lastName;
    this.age = contact.age;
    this.email = contact.email;
    this.phoneNumber = contact.phoneNumber;
    this.addresses = contact.addresses.map(
      (address) => new ResponseAddressDto(address),
    );
  }
}
