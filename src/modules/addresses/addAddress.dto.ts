import {
  IAddressDb,
  ICreateAddress,
} from '@app/core/database/collections/addresses/addresses-database.models';
import { IsString } from 'class-validator';

export class CreateAddressDto implements ICreateAddress {
  @IsString() city: string;
  @IsString() address: string;
  @IsString() postalCode: string;
}

export class ResponseAddressDto {
  id: string;
  city: string;
  address: string;
  postalCode: string;

  constructor(address: IAddressDb) {
    this.id = address.id;
    this.city = address.city;
    this.address = address.address;
    this.postalCode = address.postalCode;
  }
}
