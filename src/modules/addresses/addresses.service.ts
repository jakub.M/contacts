import { DatabaseService } from '@app/core/database/database.service';
import {
  CreateAddressDto,
  ResponseAddressDto,
} from '@app/modules/addresses/addAddress.dto';
import { Injectable } from '@nestjs/common';
import { map } from 'rxjs';

@Injectable()
export class AddressesService {
  constructor(private readonly databaseService: DatabaseService) {}

  addAddressesToContact(addresses: CreateAddressDto[], contactId: string) {
    //check if contact exits
    return this.databaseService
      .createManyAddresses(addresses, contactId)
      .pipe(
        map((addresses) =>
          addresses.map((address) => new ResponseAddressDto(address)),
        ),
      );
  }
}
