import { CreateAddressDto } from '@app/modules/addresses/addAddress.dto';
import { AddressesService } from '@app/modules/addresses/addresses.service';
import { Body, Controller, Param, Post } from '@nestjs/common';

@Controller('addresses')
export class AddressesController {
  constructor(private addressesService: AddressesService) {}
  @Post('many/to/contact/:contactId')
  createMany(
    @Body() addresses: CreateAddressDto[],
    @Param('contactId') contactId: string,
  ) {
    return this.addressesService.addAddressesToContact(addresses, contactId);
  }
}
